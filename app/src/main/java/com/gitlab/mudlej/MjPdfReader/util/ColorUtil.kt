package com.gitlab.mudlej.MjPdfReader.util

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import com.gitlab.mudlej.MjPdfReader.R
import com.google.android.material.elevation.SurfaceColors


object ColorUtil {

    @RequiresApi(Build.VERSION_CODES.M)
    fun colorize(context: Context, window: Window, actionBar: ActionBar?) {
        val color = context.getColor(R.color.e_action_bar)
        // status bar color
        window.statusBarColor = color
        window.navigationBarColor = color

        // App Bar background color
        val colorDrawable = ColorDrawable(color)
        actionBar?.setBackgroundDrawable(colorDrawable)
    }
}
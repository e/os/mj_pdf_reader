#!/bin/bash

# Original Script: https://gist.github.com/GR8DAN/da9240b26e6727f53ee4bc68108c6c65

echo "Clean a Android Studio project ready for importing and zipping pure code"
echo "See http://tekeye.uk/android/export-android-studio-project"
echo "Modify this file to meet project requirements, it only does the basics"
echo "Edited by Mudlej to run on Linux and keep tests and some gradle files"

echo "Remove Gradle code, added back in on import"
rm -rf .gradle

echo "Remove IDE files"
rm -rf .idea

echo "Remove build folders, will be recreated"
rm -rf build
rm -rf app/build
rm -rf PdfiumAndroid/build

echo "Remove libs folder"
rm -rf app/libs
rm -rf PdfiumAndroid/src/main/libs

echo "Finished."

![Feature Graphic](https://gitlab.com/mudlej_android/mj_pdf_reader/-/raw/main/app/src/main/feature_graphic.png)

# MJ PDF
MJ PDF is a fast, minimalist, powerful and totally free PDF viewer made by [Mudlej](https://gitlab.com/mudlej).


# Download & Links
- [x] [Play Store](https://play.google.com/store/apps/details?id=com.gitlab.mudlej.MjPdfReader)
- [x] [IzzyOnDroid Repo](https://apt.izzysoft.de/fdroid/index/apk/com.gitlab.mudlej.MjPdfReader)
- [x] [Direct Download (V2.2.1)](https://gitlab.com/mudlej_android/mj_pdf_reader/uploads/61d28d2434f42c02e8dca9000a21e8a9/mj-pdf-2.2.1.apk)
- [x] [Github Page for issues](https://github.com/mudlej/mj_pdf/)
- [ ] F-droid (stale request)


# TABLE OF CONTENTS
* [MJ PDF](#mj-pdf)
* [Download & Links](#download--links)
* [Screenshots](#screenshots)
* [Github Page](#github-page)
* [More Screenshots](https://gitlab.com/mudlej_android/mj_pdf_reader/-/tree/main/screenshots)
* [MJ PDF Features](#mj-pdf-features)
* [Permissions and privacy](#permissions-and-privacy)
* [MJ PDF V2.1.2 Release Notes](#mj-pdf-v212-release-notes)
* [MJ PDF TO-DO List](https://gitlab.com/mudlej_android/mj_pdf_reader/-/blob/main/todo.md)
* [What is different from PDF Viewer Plus](#what-is-different-from-pdf-viewer-plus)
* [Underlying Libraries](#underlying-libraries)
* [Authors and acknowledgment](#authors-and-acknowledgment)
* [License](#authors-and-acknowledgment)


## MJ PDF Features
- Fast, simple, and very lightweight. (~5MB in Play Store)
- Open source with total privacy.
- Remembers the last page that was opened in each document.
- Dark mode for the PDF.
- Very fast and powerful search in the PDF.
- Full-screen mode with buttons to:  
  - Rotate the screen.  
  - Brightness control bar.  
  - Auto scroll with adjustable speed.  
  - Lock horizontal swipe.  
  - Take a screenshot.
- Text Mode to view the PDF a text.
- A page to see the full Table of Content
- A page to see all the Links embedded in the PDF.
- Open online PDFs through links.
- Share & print PDFs.
- Open multiple instance of the app at the same time.


## Screenshots
| Light Mode | Dark Mode | Main Menu |
|:-:|:-:|:-:|
| ![Light Mode](https://gitlab.com/mudlej_android/mj_pdf_reader/-/raw/main/screenshots/light_framed.png) | ![Dark Mode](https://gitlab.com/mudlej_android/mj_pdf_reader/-/raw/main/screenshots/dark_framed.png) | ![Main Menu](https://gitlab.com/mudlej_android/mj_pdf_reader/-/raw/main/screenshots/light_main_menu_framed.png) |


## Github Page
The codebase is hosted on [Gitlab](https://gitlab.com/mudlej_android/mj_pdf_reader). But I opened a page in Github for issues like requests, bug reports...
[Github page link](https://github.com/mudlej/mj_pdf/).


## Permissions and privacy
This app does not collect any data.
The following permissions are required to provide specific features in the app:
* *Internet*: For opening PDFs through links
* *Storage*: For saving downloading PDFs and opening them from storage

## MJ PDF V2.2.1 Short Release Notes
* Added support for Arabic, Chinese, Turkish, German, Spanish, Portuguese, Hindi, and Russian.
* Introduced Always in Horizontal Mode option.
* Added return button to search results, improved search result highlighting.
* Added full support for password-protected and non-local PDF.
* Added an experimental setting to show a 'reload PDF file' button
* Fixed issues with the back button not functioning in some cases.
* Improved UI consistency with icon and text colors, and a more streamlined Full Screen Buttons layout.
* Addressed many crash scenarios.
* Updated to Android SDK, Gradle and many deps.

## What is different from PDF Viewer Plus
After the launch of MJ PDF, Gokul Swaminathan discontinued PDF Viewer Plus app. 
And he [suggested](https://github.com/JavaCafe01/PdfViewer#anouncement) MJ PDF as a replacement.
MJ PDF V2.0 codebase is 400% larger than PDF Viewer Plus without counting the libraries forked for MJ PDF, while being a quarter of its size.
[See changelog](https://gitlab.com/mudlej_android/mj_pdf_reader/-/blob/main/change_log.md)

## Underlying Libraries
I Forked [PdfiumAndroid](https://github.com/barteksc/PdfiumAndroid) to update its core libraries that were years behind and had too many security vulnerabilities.
And Forked [ AndroidPdfViewer](https://github.com/barteksc/AndroidPdfViewer) to add features (like extracting PDF text) and modify some of its behavior (like scroll handle).

* Updated PDFium to 112.0.5579.0 (in v2.0.2) ([source code](https://pdfium.googlesource.com/pdfium/+/refs/heads/main), [building script](https://github.com/bblanchon/pdfium-binaries))
* Updated libpng to 1.6.39 ([source code](https://sourceforge.net/projects/libpng/files/libpng16/1.6.37/), [building script](https://gitlab.com/mudlej_android/mj_pdf_reader/-/blob/main/build_dependencies/libpng.py))
* Updated Freetype to 2.13.0 ([source code](https://github.com/freetype/freetype), [building script](https://gitlab.com/mudlej_android/mj_pdf_reader/-/blob/main/build_dependencies/freetype2.py))

## How to Build the native part in PdfiumAndroid
This is necessary to compile the code in `mainJNILib.cpp`, ensuring that any modifications made are applied effectively. 
Go to `mj_pdf_reader/PdfiumAndroid/src/main/jni` and run command `$ ndk-build`.
Every future `.aar` or `apk` build will use the generated libs.

## Authors and acknowledgment
- MJ PDF is made by [Mudlej](https://gitlab.com/mudlej).
- The original app (PDF View Plus) was made by Gokul Swaminathan ([@JavaCafe01](https://github.com/JavaCafe01)).
- [@barteksc](https://github.com/barteksc), made the libraries that MJ PDF uses to render PDFs. 
- Credits to (@Derekelkins)'s pull request on Pdf Viewer Plus for adding the ability to remember last opened page.
- Big thanks to [Bnyro](https://gitlab.com/Bnyro) (LibreTube's dev) for helping me with the colors and how to migrate to M3. (MJ PDF v2.1)

## License
MJ PDF uses the GPLv3 license, the original app (PDF View Plus) was under MIT license